#!/bin/bash

# usage: ./minio-upload my-bucket my-file.zip

bucket=$1
file=$2

. /usr/local/etc/minio.env

#host=minio.example.com
#s3_access_key='secret key'
#s3_secret_key='secret token'

resource="/${bucket}/${file}"

content_type="application/octet-stream"
date=$(date -R)
_signature="PUT\n\n${content_type}\n${date}\n${resource}"
signature=$(echo -en ${_signature} | openssl sha1 -hmac ${s3_secret_key} -binary | base64)

# TODO
# check with HEAD

curl \
  --silent \
  -X PUT \
  -T "${file}" \
  -H "Host: ${host}" \
  -H "Date: ${date}" \
  -H "Content-Type: ${content_type}" \
  -H "Authorization: AWS ${s3_access_key}:${signature}" \
  http://${host}${resource}

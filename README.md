# ansible role to build coremedia artefacts

**you need an qualified CoreMedia account to download the Artefacts for an successfully build**

## dependencies

- [sencha_cmd](https://gitlab.com/coremedia-as-code/ansible-roles/ansible-sencha_cmd)
- [maven](https://gitlab.com/coremedia-as-code/ansible-roles/ansible-maven)
- [chromium-headless](https://gitlab.com/coremedia-as-code/ansible-roles/ansible-chromium-headless)
- [nodejs](https://github.com/geerlingguy/ansible-role-nodejs)
- [docker](https://github.com/geerlingguy/ansible-role-docker)
- [java](https://github.com/geerlingguy/ansible-role-java)


## Role Variables

[defaults/main.yml](defaults/main.yml)

|*Variable*  | *Default Value* | *Description* |
| --- | --- | --- |
| `coremedia_build_release` | `''` | CoreMedia release (e.g. *cms-9* or *cmcc-10*) |
| `coremedia_build_workspace_version` | `` | CoreMedia Workspace (e.g. *1801.4*, *1910.1* or *2007.1*) |
| `coremedia_build_release.download_server` | `https://releases.coremedia.com` | URL to get released artefacts |
| `coremedia_build_release.download_username` | `''` | Login Credentials for `coremedia_build_release.download_server` |
| `coremedia_build_release.download_password` | `''` | Login Credentials for `coremedia_build_release.download_server` |
| `coremedia_build_force` | `false` | force build, e.g. removes all existing docker containers |
| `coremedia_build_docker` | `true` | |
| `coremedia_build_chef` | `false` | |

```
coremedia_build_disable_extension:
  elastic_social: true
  adaptive_personalization: true
  ecommerce: true
  advanced_asset_management: true
```

```
coremedia_build_use_image_tag: 'latest'
coremedia_build_use_repository_prefix: ''
```

```
coremedia_build_use_docker_base_image:
  repo: 'harbor.cm.local/coremedia/java-application-base'
  tag: '2.2.1-openjdk-11-jre'
```

```
coremedia_build.docker_registry:
  host: harbor.cm.local
  user: coremedia
  password: C0remedia
```

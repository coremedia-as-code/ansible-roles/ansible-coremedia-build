---

- name: create local temporary path
  become: false
  file:
    path: "{{ coremedia_build_local_tmp_directory }}"
    state: directory
    mode: 0700
  delegate_to: localhost

- name: 'download workspace from {{ coremedia_build_workspace_download_url }} to local folder'
  become: false
  get_url:
    url: "{{ coremedia_build_workspace_download_url }}"
    url_username: "{{ coremedia_build_release.download_username }}"
    url_password: "{{ coremedia_build_release.download_password }}"
    dest: "{{ coremedia_build_local_tmp_directory }}/{{ coremedia_build_workspace_archive_name }}"
  register: download_workspace_archive
  until: download_workspace_archive is succeeded
  retries: 5
  delay: 2
  delegate_to: localhost
  check_mode: false
  tags:
    - download

- name: 'download blobarchive from {{ coremedia_build_blobarchive_download_url }} to local folder'
  become: false
  get_url:
    url: '{{ coremedia_build_blobarchive_download_url }}'
    url_username: "{{ coremedia_build_release.download_username }}"
    url_password: "{{ coremedia_build_release.download_password }}"
    dest: '{{ coremedia_build_local_tmp_directory }}/{{ coremedia_build_blobarchive_name }}'
  register: download_blobarchive_archive
  until: download_blobarchive_archive is succeeded
  retries: 5
  delay: 2
  delegate_to: localhost
  check_mode: false
  tags:
    - download

- name: deploy workspace archive {{ coremedia_build_workspace_archive_name }}
  copy:
    src: "{{ coremedia_build_local_tmp_directory }}/{{ coremedia_build_workspace_archive_name }}"
    dest: "{{ coremedia_build_tmp_directory }}/{{ coremedia_build_workspace_archive_name }}"
    owner: "{{ coremedia_build_user }}"
    group: "{{ coremedia_build_user }}"
    mode: 0666

- name: deploy blobarchive for workspace {{ coremedia_build_workspace_version }}
  copy:
    src: "{{ coremedia_build_local_tmp_directory }}/{{ coremedia_build_blobarchive_name }}"
    dest: "{{ coremedia_build_deployment_base_directory }}/{{ coremedia_build_blobarchive_name }}"
    owner: "{{ coremedia_build_user }}"
    group: "{{ coremedia_build_user }}"
    mode: 0666

- name: '[fact] set workspace_archive_state'
  set_fact:
    workspace_archive_state: true
  when:
    - download_workspace_archive is defined
    - not download_workspace_archive.failed

- name: '[fact] set workspace_archive_state'
  set_fact:
    blobarchive_archive_state: true
  when:
    - download_blobarchive_archive is defined
    - not download_blobarchive_archive.failed

- name: 'extract workspace archive'
  unarchive:
    src: "{{ coremedia_build_tmp_directory }}/{{ coremedia_build_workspace_archive_name }}"
    dest: "{{ coremedia_build_tmp_directory }}"
    owner: "{{ coremedia_build_user }}"
    group: "{{ coremedia_build_user }}"
    copy: false
  no_log: true
  when:
    - workspace_archive_state is defined
    - workspace_archive_state

- name: extract blobarchive archive
  unarchive:
    src: '{{ coremedia_build_deployment_base_directory }}/{{ coremedia_build_blobarchive_name }}'
    dest: '{{ coremedia_build_tmp_directory }}'
    owner: "{{ coremedia_build_user }}"
    group: "{{ coremedia_build_user }}"
    copy: false
  no_log: true
  when:
    - blobarchive_archive_state is defined
    - blobarchive_archive_state

- name: 'stat {{ coremedia_build_content_users_zip }}'
  stat:
    path: '{{ coremedia_build_content_users_zip }}'
    get_checksum: false
    get_md5: false
    get_mime: false
    get_attributes: false
  register: __stat_content_users_archive
  changed_when: not __stat_content_users_archive.stat.exists

- name: 'stat {{ coremedia_build_deployment_base_directory }}/chef-repo'
  stat:
    path: "{{ coremedia_build_deployment_base_directory }}/chef-repo"
    get_checksum: false
    get_md5: false
    get_mime: false
    get_attributes: false
  register: __stat_chef_repo
  changed_when: not __stat_chef_repo.stat.exists

- name: 'set content_users_state'
  set_fact:
    content_users_state: true
  when:
    - __stat_content_users_archive.stat.exists

- name: 'set chef_repo_state'
  set_fact:
    chef_repo_state: true
  when:
    - __stat_chef_repo.stat.exists

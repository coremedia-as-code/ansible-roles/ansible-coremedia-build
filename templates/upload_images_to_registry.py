#!/usr/bin/env python3

import docker
# client = docker.from_env()

#client = docker.APIClient(base_url='unix:/run/docker.sock')
#print(client.version())

client = docker.DockerClient(base_url='unix://run/docker.sock')


#print( client.containers(all=True) )

#print( client.images.list(all=True) )

#for container in client.containers.list(all=True, filters={"name":"coremedia"}):
#  print( container.name)


for i in client.images.list():
  print( " - {} - {}".format( i.short_id, i.tags ) )

